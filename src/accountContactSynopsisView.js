/**
 * The least detailed view of a account contact
 * @class {AccountContactSynopsisView}
 */
export default class AccountContactSynopsisView{

    _accountId:string;

    _id:string;

    _firstName:string;

    _lastName:string;

    _phoneNumber:string;

    _emailAddress:string;

    /**
     * @param {string} accountId
     * @param {string} id
     * @param {string} firstName
     * @param {string} lastName
     * @param {string} phoneNumber
     * @param {string} emailAddress
     */
    constructor(
        accountId:string,
        id:string,
        firstName:string,
        lastName:string,
        phoneNumber:string,
        emailAddress:string
    ){

        if(!accountId){
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

        if(!id){
            throw new TypeError('id required');
        }
        this._id = id;

        if(!firstName){
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if(!lastName){
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if(!phoneNumber){
            throw new TypeError('phoneNumber required');
        }
        this._phoneNumber = phoneNumber;

        if(!emailAddress){
            throw new TypeError('emailAddress required');
        }
        this._emailAddress = emailAddress;

    }

    /**
     * @returns {string}
     */
    get accountId():string {
        return this._accountId;
    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get firstName():string {
        return this._firstName;
    }

    /**
     * @returns {string}
     */
    get lastName():string {
        return this._lastName;
    }

    /**
     * @returns {string}
     */
    get phoneNumber():string {
        return this._phoneNumber;
    }

    /**
     * @returns {string}
     */
    get emailAddress():string {
        return this._emailAddress;
    }
}
