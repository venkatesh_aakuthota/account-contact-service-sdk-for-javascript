import {inject} from 'aurelia-dependency-injection';
import AccountContactServiceSdkConfig from './accountContactServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AccountContactSynopsisView from './accountContactSynopsisView';

@inject(AccountContactServiceSdkConfig, HttpClient)
class ListAccountContactsWithAccountIdFeature {

    _config:AccountContactServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Lists all account contacts with the provided account id
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {Promise.<AccountContactSynopsisView[]>}
     */
    execute(accountId:string,
            accessToken:string):Promise<Array> {


        return this._httpClient
            .createRequest(`account-contacts`)
            .asGet()
            .withBaseUrl(this._config.baseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({
                accountId
            })
            .send(
            )
            .then(response =>
                Array.from(
                    response.content,
                    contentItem =>
                        new AccountContactSynopsisView(
                            contentItem.accountId,
                            contentItem.id,
                            contentItem.firstName,
                            contentItem.lastName,
                            contentItem.phoneNumber,
                            contentItem.emailAddress
                        )
                )
            );
    }
}

export default ListAccountContactsWithAccountIdFeature;