import {inject} from 'aurelia-dependency-injection';
import AccountContactServiceSdkConfig from './accountContactServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import AddAccountContactReq from './addAccountContactReq';

@inject(AccountContactServiceSdkConfig, HttpClient)
class AddAccountContactFeature {

    _config:AccountContactServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Adds a accountContact
     * @param {AddAccountContactReq} request
     * @param accessToken
 * @returns {Promise.<string>} id
     */
    execute(request:AddAccountContactReq,
            accessToken:string):Promise<string> {

        return this._httpClient
            .createRequest('account-contacts')
            .asPost()
            .withBaseUrl(this._config.baseUrl)
            .withHeader('Accept', 'text/plain')
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then((response) => response.content);
    }
}

export default AddAccountContactFeature;