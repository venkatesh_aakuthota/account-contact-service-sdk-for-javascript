import {inject} from 'aurelia-dependency-injection';
import AccountContactServiceSdkConfig from './accountContactServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AccountContactView from './accountContactView';

@inject(AccountContactServiceSdkConfig, HttpClient)
class GetAccountContactWithIdFeature {

    _config:AccountContactServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Gets the accountContact with the provided id
     * @param {string} id
     * @param {string} accessToken
     * @returns {Promise.<AccountContactView>}
     */
    execute(id:string,
            accessToken:string):Promise<AccountContactView> {

        return this._httpClient
            .createRequest(`account-contacts/${id}`)
            .asGet()
            .withBaseUrl(this._config.baseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => response.content)
            .then((content) =>
                new AccountContactView(
                    content.accountId,
                    content.id,
                    content.firstName,
                    content.lastName,
                    content.phoneNumber,
                    content.emailAddress
                ));
    }
}

export default GetAccountContactWithIdFeature;