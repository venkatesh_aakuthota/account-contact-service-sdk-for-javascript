export default class AddAccountReq {

    _accountId:string;

    _firstName:string;

    _lastName:string;

    _phoneNumber:string;

    _emailAddress:string;

    _countryIso31661Alpha2Code:string;

    _regionIso31662Code:string;

    /**
     * @param {string} accountId
     * @param {string} firstName
     * @param {string} lastName
     * @param {string} phoneNumber
     * @param {string} emailAddress
     * @param {string} countryIso31661Alpha2Code
     * @param {string} regionIso31662Code
     */
    constructor(accountId:string,
                firstName:string,
                lastName:string,
                phoneNumber:string,
                emailAddress:string,
                countryIso31661Alpha2Code:string,
                regionIso31662Code:string) {

        if (!accountId) {
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if (!phoneNumber) {
            throw new TypeError('phoneNumber required');
        }
        this._phoneNumber = phoneNumber;

        if (!emailAddress) {
            throw new TypeError('emailAddress required');
        }
        this._emailAddress = emailAddress;

        if (!countryIso31661Alpha2Code) {
            throw new TypeError('countryIso31661Alpha2Code required');
        }
        this._countryIso31661Alpha2Code = countryIso31661Alpha2Code;

        if (!regionIso31662Code) {
            throw new TypeError('regionIso31662Code required');
        }
        this._regionIso31662Code = regionIso31662Code;

    }

    /**
     * @returns {string}
     */
    get accountId():string {
        return this._accountId;
    }

    /**
     * @returns {string}
     */
    get firstName():string {
        return this._firstName;
    }

    /**
     * @returns {string}
     */
    get lastName():string {
        return this._lastName;
    }

    /**
     * @returns {string}
     */
    get phoneNumber():string {
        return this._phoneNumber;
    }

    /**
     * @returns {string}
     */
    get emailAddress():string {
        return this._emailAddress;
    }

    /**
     * @returns {string}
     */
    get countryIso31661Alpha2Code():string {
        return this._countryIso31661Alpha2Code;
    }

    /**
     * @returns {string}
     */
    get regionIso31662Code():string {
        return this._regionIso31662Code;
    }

    toJSON() {
        return {
            accountId: this._accountId,
            firstName: this._firstName,
            lastName: this._lastName,
            phoneNumber: this._phoneNumber,
            emailAddress: this._emailAddress,
            countryIso31661Alpha2Code: this._countryIso31661Alpha2Code,
            regionIso31662Code: this._regionIso31662Code
        }
    }

}
