/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default {
    firstName: 'firstName',
    lastName: 'lastName',
    phoneNumber: '0000000000',
    emailAddress: 'fake-email@test.com',
    iso31661Alpha2Code: 'US',
    iso31662Code:'WA',
    url: 'https://test-url.com',
    sapAccountNumber: 'sapAccountNo',
    sapVendorNumber: 'sapVendorNo',
    partnerRepId: 1,
    accountId:'000000000000000000',
    accountContactId:'000000000000000000'
};
