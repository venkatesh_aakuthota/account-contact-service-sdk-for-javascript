import {AddAccountContactReq} from '../../src/index';
import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';

export default {
    constructValidPartnerRepOAuth2AccessToken,
    constructValidAddAccountContactRequest
}

function constructValidPartnerRepOAuth2AccessToken(accountId:string = dummy.accountId):string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'partnerRep',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "email": dummy.emailAddress,
        "sub": `${dummy.partnerRepId}`,
        "account_id": accountId,
        "sap_vendor_number": dummy.sap_vendor_number
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructValidAddAccountContactRequest(accountId) {
    return new AddAccountContactReq(
        accountId,
        dummy.firstName,
        dummy.lastName,
        dummy.phoneNumber,
        dummy.emailAddress,
        dummy.iso31661Alpha2Code,
        dummy.iso31662Code
    );
}
