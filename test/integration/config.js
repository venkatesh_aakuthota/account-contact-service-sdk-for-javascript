import AccountContactServiceSdkConfig from '../../src/accountContactServiceSdkConfig';

export default {
    accountContactServiceSdkConfig: new AccountContactServiceSdkConfig(
        'https://account-contact-service-dev.precorconnect.com'
    ),
    identityServiceJwtSigningKey: 'nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ',
    idOfExistingPartnerAccountAssociation: {
        accountId: '001K000001GMV14IAH',
        partnerAccountId: '001K000001GMV0zIAH'
    }
};