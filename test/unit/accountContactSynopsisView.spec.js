import AccountContactSynopsisView from '../../src/accountContactSynopsisView';
import dummy from '../dummy';

/*
 test methods
 */
describe('AccountContactSynopsisView class', () => {
    describe('constructor', () => {
        it('throws if accountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AccountContactSynopsisView(
                        null,
                        dummy.accountContactId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.phoneNumber,
                        dummy.emailAddress
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'accountId required');
        });
        it('sets accountId', () => {
            /*
             arrange
             */
            const expectedAccountId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new AccountContactSynopsisView(
                    expectedAccountId,
                    dummy.accountContactId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.phoneNumber,
                    dummy.emailAddress
                );

            /*
             assert
             */
            const actualAccountId = objectUnderTest.accountId;
            expect(actualAccountId).toEqual(expectedAccountId);
        });
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AccountContactSynopsisView(
                        dummy.accountId,
                        null,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.phoneNumber,
                        dummy.emailAddress
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.accountContactId;

            /*
             act
             */
            const objectUnderTest =
                new AccountContactSynopsisView(
                    dummy.accountId,
                    expectedId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.phoneNumber,
                    dummy.emailAddress
                );

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);
        });
        it('throws if firstName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AccountContactSynopsisView(
                        dummy.accountId,
                        dummy.accountContactId,
                        null,
                        dummy.lastName,
                        dummy.phoneNumber,
                        dummy.emailAddress
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'firstName required');
        });
        it('sets firstName', () => {
            /*
             arrange
             */
            const expectedFirstName = dummy.firstName;

            /*
             act
             */
            const objectUnderTest =
                new AccountContactSynopsisView(
                    dummy.accountId,
                    dummy.accountContactId,
                    expectedFirstName,
                    dummy.lastName,
                    dummy.phoneNumber,
                    dummy.emailAddress
                );

            /*
             assert
             */
            const actualFirstName = objectUnderTest.firstName;
            expect(actualFirstName).toEqual(expectedFirstName);
        });
        it('throws if lastName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AccountContactSynopsisView(
                        dummy.accountId,
                        dummy.accountContactId,
                        dummy.firstName,
                        null,
                        dummy.phoneNumber,
                        dummy.emailAddress
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'lastName required');
        });
        it('sets lastName', () => {
            /*
             arrange
             */
            const expectedLastName = dummy.lastName;

            /*
             act
             */
            const objectUnderTest =
                new AccountContactSynopsisView(
                    dummy.accountId,
                    dummy.accountContactId,
                    dummy.firstName,
                    expectedLastName,
                    dummy.phoneNumber,
                    dummy.emailAddress
                );

            /*
             assert
             */
            const actualLastName = objectUnderTest.lastName;
            expect(actualLastName).toEqual(expectedLastName);
        });
        it('throws if phoneNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AccountContactSynopsisView(
                        dummy.accountId,
                        dummy.accountContactId,
                        dummy.firstName,
                        dummy.lastName,
                        null,
                        dummy.emailAddress
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'phoneNumber required');
        });
        it('sets phoneNumber', () => {
            /*
             arrange
             */
            const expectedPhoneNumber = dummy.phoneNumber;

            /*
             act
             */
            const objectUnderTest =
                new AccountContactSynopsisView(
                    dummy.accountId,
                    dummy.accountContactId,
                    dummy.firstName,
                    dummy.lastName,
                    expectedPhoneNumber,
                    dummy.emailAddress
                );

            /*
             assert
             */
            const actualPhoneNumber = objectUnderTest.phoneNumber;
            expect(actualPhoneNumber).toEqual(expectedPhoneNumber);
        });
        it('throws if emailAddress is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AccountContactSynopsisView(
                        dummy.accountId,
                        dummy.accountContactId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.phoneNumber,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'emailAddress required');
        });
        it('sets emailAddress', () => {
            /*
             arrange
             */
            const expectedEmailAddress = dummy.emailAddress;

            /*
             act
             */
            const objectUnderTest =
                new AccountContactSynopsisView(
                    dummy.accountId,
                    dummy.accountContactId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.phoneNumber,
                    expectedEmailAddress
                );

            /*
             assert
             */
            const actualEmailAddress = objectUnderTest.emailAddress;
            expect(actualEmailAddress).toEqual(expectedEmailAddress);
        });
    });
});
