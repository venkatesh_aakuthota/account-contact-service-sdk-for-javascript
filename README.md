## Description
Precor Connect account contact service SDK for javascript.

## Features

##### Add Account Contact
* [documentation](features/AddAccountContact.feature)

##### Get Account Contact With Id
* [documentation](features/GetAccountContactWithId.feature)

##### List Account Contacts With Account Id
* [documentation](features/ListAccountContactsWithAccountId.feature)

## Setup

**install via jspm**  
```shell
jspm install account-contact-service-sdk=bitbucket:precorconnect/account-contact-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import AccountContactServiceSdk,{AccountContactServiceSdkConfig} from 'account-contact-service-sdk'

const accountContactServiceSdkConfig = 
    new AccountContactServiceSdkConfig(
        "https://account-contact-service-dev.precorconnect.com"
    );
    
const accountContactServiceSdk = 
    new AccountContactServiceSdk(
        accountContactServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```